require("dotenv").config();
const {
	Telegraf,
	Markup,
	session,
	Scenes: { WizardScene, Stage },
} = require("telegraf");
const { min, max } = require("./constants");

const bot = new Telegraf(process.env.BOT_TOKEN);

const number = Math.round(Math.random() * (max - min) + min);
let attempts = 5;

const game_keyboard = Markup.inlineKeyboard([
	Markup.button.callback("Играть в игру", "Играть в игру"),
]);
const about_keyboard = Markup.keyboard(["О боте"]);
const again_keyboard = Markup.inlineKeyboard([
	Markup.button.callback("Играть еще раз", "Играть еще раз"),
	Markup.button.callback("В главное меню", "В главное меню"),
]);
const main_keyboard = Markup.keyboard(["В главное меню"]);
const remove_keyboard = Markup.removeKeyboard();

const gameHandler = Telegraf.on("text", async (ctx) => {
	ctx.scene.state.current = ctx.message.text;

	console.log(ctx.scene.state.current, number, attempts, 0);
	if (ctx.scene.state.current !== number && attempts === 1) {
		console.log("Попытки закончились");
		await ctx.reply(
			"Ты проиграл, давай попробуем еще раз?",
			again_keyboard
		);

		return ctx.scene.leave();
	}

	if (ctx.scene.state.current < 1 || ctx.scene.state.current > 15) {
		console.error("Число не валидное");
		await ctx.reply(
			`Число должно быть больше 1 и меньше 15`,
			remove_keyboard
		);
		return;
	}

	if (ctx.scene.state.current !== number) {
		attempts = attempts - 1;
		console.log("Число не угадано");
		await ctx.reply(
			`Не верно (осталось ${attempts} попыток)`,
			remove_keyboard
		);
		return;
	}

	if (ctx.scene.state.current === number) {
		console.log("Число угадано");
		return ctx.reply(
			"Молодец. У тебя хорошая интуиция)",
			again_keyboard,
			main_keyboard
		);
	}
});

const gameScene = new WizardScene("gameScene", gameHandler);
gameScene.enter((ctx) =>
	ctx.reply(
		"Привет я запомнил одно число от 1 до 15. У тебя есть 5 попытки что бы угадать это число. Если угадаешь от меня комплимент)"
	)
);

const stage = new Stage([gameScene]);
bot.use(session(), stage.middleware());

bot.action("Играть в игру", (ctx) => {
	console.log(ctx, "ctx");
	return ctx.scene.enter("gameScene");
});
bot.action("Играть еще раз", (ctx) => {
	attempts = 5;
	return ctx.scene.enter("gameScene");
});
bot.action("В главное меню", (ctx) => ctx.reply(game_keyboard, about_keyboard));

bot.command("/help", (ctx) => ctx.reply('Это бот по игре "Угадай число"'));
bot.command("/start", (ctx) =>
	ctx.reply("Hello", game_keyboard, about_keyboard)
);
bot.launch();

// Enable graceful stop
process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));

console.log("Bot started");
