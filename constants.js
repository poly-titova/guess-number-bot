const commands = `
/start - Перезапустить бота
/help - Помощь`;

const min = 1;
const max = 15;

module.exports = {
  commands,
  min,
  max,
};
